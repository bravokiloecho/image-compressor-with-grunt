module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        imagemin: { // Task

            dynamic: { // Another target
                options: {
                    cache: false,
                    optimizationLevel: 2,
                    pngquant: true
                },
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'gfx/', // Src matches are relative to this path
                    src: ['*.{png,jpg,gif}','*/*.{png,jpg,gif}'], // Actual patterns to match
                    dest: 'gfx_min/' // Destination path prefix
                }]
            }
        },



        image_resize: {
            options: {
              width: 800,
              overwrite: false
            },
            your_target: {
              files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: 'gfx', // Src matches are relative to this path
                    src: ['*.{png,jpg,gif}','*/*.{png,jpg,gif}'], // Actual patterns to match
                    dest: 'gfx_resized/' // Destination path prefix
                }] 
            },
        }

    });

    // Load grunt plugins.
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-image-resize');


    //register tasks
    grunt.registerTask('default', ['image_resize']);
    grunt.registerTask('m', ['imagemin:dynamic']);
    grunt.registerTask('r', ['image_resize']);

};